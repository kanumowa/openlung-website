const routes = require('../constants')

module.exports = [
  {
    text: 'Developers',
    link: '/developers/'
  },
  {
    text: 'Information',
    link: routes.content
  }
]
