const { fs, path } = require('@vuepress/shared-utils')
const routes = require('./constants')

module.exports = ctx => ({
  base: '/openlung-website/',
  dest: 'public',
  locales: {
    '/': {
      lang: 'en-US',
      title: 'OpenLung',
      description: 'Ventilator documentation for OpenLung '
    }
  },
  head: [
    ['link', { rel: 'icon', href: `/logo.png` }],
    ['link', { rel: 'manifest', href: '/manifest.json' }],
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }],
    ['link', { rel: 'apple-touch-icon', href: `/icons/apple-touch-icon-152x152.png` }],
    ['link', { rel: 'mask-icon', href: '/icons/safari-pinned-tab.svg', color: '#3eaf7c' }],
    ['meta', { name: 'msapplication-TileImage', content: '/icons/msapplication-icon-144x144.png' }],
    ['meta', { name: 'msapplication-TileColor', content: '#000000' }]
  ],
  themeConfig: {
    editLinks: true,
    docsDir: '',
    search: false,
    /*algolia: ctx.isProd ? ({
      apiKey: '',
      indexName: 'vuepress'
    }) : null,*/
    smoothScroll: true,
    locales: {
      '/': {
        label: 'English',
        selectText: 'Languages',
        ariaLabel: 'Select language',
        editLinkText: 'Suggest edits this page',
        lastUpdated: 'Last Updated',
        nav: require('./nav/en'),
        sidebar: {
          '/developers/': getDevelopersSidebar('Website'),
          [routes.content]: getSidebar(),
        }
      },
    }
  },
  plugins: [
    ['@vuepress/back-to-top', true],
    ['@vuepress/pwa', {
      serviceWorker: true,
      updatePopup: true
    }],
    ['@vuepress/medium-zoom', true],
    ['flowchart']
  ],
  extraWatchFiles: [
    '.vuepress/nav/en.js',
  ]
})


function getDevelopersSidebar (sectionA) {
  return [
    {
      title: sectionA,
      collapsable: false,
      children: [
        '',
      ]
    }
  ]
}

function getSidebar() {
  return [
    {
      title: 'Ventilate Now! Quick Start',
      path: `${routes.content}ventilateNow`,
      sidebarDepth: 6,
      children: [
        `${routes.content}ventilateNow`
      ]
    },
    {
      title: 'Other Devices & Tech Help',
      sidebarDepth: 6,
      path: `${routes.content}otherDevices`,
      children: [
        `${routes.content}otherDevices`
      ]
    },
    {
      title: 'Maker Resources',
      sidebarDepth: 6,
      path: `${routes.content}makerResources`,
      children: [
        `${routes.content}makerResources`
      ]
    },
    {
      title: 'Help us',
      sidebarDepth: 6,
      path: `${routes.content}helpUs`,
      children: [
        `${routes.content}helpUs`
      ]
    },
    {
      title: 'OSV',
      sidebarDepth: 6,
      path: `${routes.content}osv`,
      children: [
        `${routes.content}osv`
      ]
    },
  ]
}