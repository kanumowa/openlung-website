# Getting Started

The OpenLung website is built using [Vuepress](https://vuepress.vuejs.org/). Getting started contributing is simple.

## Installing

1. If you don't have one already, create an account on [Gitlab](https://gitlab.com/users/sign_up)
2. Clone the github repository with `git clone https://gitlab.com/open-source-ventilator/openlung-website`
3. If you don't have Vuepress installed yet, you can install it globally using `yarn global add vuepress`
4. Once Vuepress is installed, navigate to the website checkout and run `npm run dev` to launch a local instance at `https://localhost:8080`

At this point you are good to go!

## Adding and Editing Pages

Adding pages is simple. Each page is a markdown file. So find the section folder you want to add a page to and create a new markdown file. The filename will be its url path. So if you want a page at `/makers/3d-designs` you would make the file `/makers/3d-designs.md`.

If you are creating a new section, the root or index page of each section must be named `README.md`.

## Adding pages to sidebar

If your page is going to appear in the sidebar, then the page title will be the sidebar link label.

To add a page to the sidebar, open up `/.vuepress/config.js` and navigate the `sidebar` section of the locale you are working with. There you will see sections that have a function callback for the sidebar content. We use functions here so different locales can have translated section headers.

Find the function for the sidebar you want to add a page to such as `getDevelopersSidebar` and add your file name to the childrens list. For example if you were creating a page `/developers/dev-ops.md` you would add `dev-ops` to the childrens list.

## Submitting a Merge Request

To get your code published you first have to commit to a feature branch and then create a merge request.

1. Create a feature branch from the latest master with `git checkout -b feature-branch-name`. If you are working on a specific issue then you can use the issue number as the branch name such as `git checkout -b issue/4`
2. Once you have a feature branch, commit your work. Make sure you only commit the changes that are a part of your issue. For example don't `git add -A`.
3. Push your branch up with `git push origin issue/4` using the feature branch name you picked.
4. Head over to https://gitlab.com/open-source-ventilator/openlung-website/-/merge_requests and click the "New merge request" button.
5. Source your branch and target master.
6. Once the merge request is created, you can past a link to it in the #software-apps slack channel to get more visbility for others to review.
