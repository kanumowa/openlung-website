---
home: true
heroText: Open Source Ventilator
tagline: We are a team of volunteers who are working day and night at the moment to develop a low-cost and open-source ventilator to help save lives and aid the recovery of COVID19 patients.
actionText: "User Story 1 →"
actionLink: /
footer: MIT Licensed | Copyright © 2020
---

<div class="features">
  <div class="feature">
    <h2><a href="">User Story 2→</a></h2>
    <p>Proin sagittis quis diam eget pretium. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
  </div>
  <div class="feature">
    <h2><a href="">User Story 3 →</a></h2>
    <p>Fusce finibus maximus est condimentum facilisis. Etiam eu mauris elit. In pulvinar eros a justo fringilla, sit amet pretium ante vulputate.</p>
  </div>
    <div class="feature">
    <h2><a href="">User Story 4 →</a></h2>
    <p>Nunc lobortis ante sit amet enim bibendum mattis. Vestibulum ut congue ipsum, eu imperdiet dui.</p>
  </div>
</div>

<div class="danger custom-block">
  <h2 class="custom-block-title">Attention</h2>
  <h3>Currently everything in this repository is a work in progress.</h3>
</div>

